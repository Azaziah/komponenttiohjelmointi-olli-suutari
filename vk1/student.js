// Yhden luokan exporttaus
// tiedostosta person.js
"use strict";

var Person = require('./person.js');

class Student extends Person {
  constructor(firstname, lastname, usedFirstName, birthYear, studentId, email) {
    super (firstname, lastname, usedFirstName, birthYear);
    this.studentId = studentId;
    this.email = email;
    console.log("A new student has been created.")
  }

    // Getterit ja setterit.
    set_studentId(studentId){
      this.studentId = studentId;
      console.log("A new student ID added.")
    }

    get_studentId() {
        return this.studentId;
    }

    set_email(email){
        this.email = email;
        console.log("Email was changed.")
    }

    get_email() {
        return this.email;
    }

    // Tulostetaan opiskelijan tiedot.
    introduce() {
      super.introduce();
      console.log("I'm also a student, my ID is: ", this.get_studentId() + ' and my email is: ' + this.get_email());
    }
  // Luokka loppuu
}

module.exports = Student;