var Student = require('./student.js');

// Luodaan uusi opiskelija seuraavilla tiedoilla.
var student = new Student("Matti Petteri", "Mahti", "Matti", "1993", "00000", "123456@edu.karelia.fi");
// Muutetaan opiskelijan Id jälkikäteen.
student.set_studentId(123456);
// Haetaan opiskelijan tiedot.
student.introduce();