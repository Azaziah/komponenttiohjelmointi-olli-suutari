// Yhden luokan exporttaus
// tiedostosta person.js
"use strict";

class Person {
  constructor(firstName, lastName, usedFirstName, birthYear) {
    this.firstname = firstName;
    this.lastname = lastName;
    this.usedFirstName = usedFirstName;
    this.birthYear = birthYear;
  }

  introduce() {
    console.log(this.get_PersonDetails());
  }

  get_Name() {
    return this.firstname + " " + this.lastname;
  }
  get_PersonDetails()
  {
    return '\nHello my name is: ' +  this.get_Name() + "\nI use: " + this.usedFirstName +
    ' as my calling name.\nI was born in: ' + this.birthYear; 
  }
}

module.exports = Person;