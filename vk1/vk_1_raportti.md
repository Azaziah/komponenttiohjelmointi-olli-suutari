### Olli Suutari | Komponenttiohjelmointi vk 1


### Viikon 1 raportti

Tällä viikolla harjoittelimme luokkien, aliluokkien ja gettereiden ja settereiden käyttöä. Asentelimme myös kehitysympäristöt kuntoon.

Viikkotehtävänämme oli luoda luokka "henkilö" sekä tälle aliluokka "opiskelija" gettereine ja settereineen.

Sain ymmärtääkseni tehtyä tehtävän vaatimusten mukaisesti.

Node ja Javascript ovat minulle jo ennestään jokseenkin tuttuja, viikkotehtävä oli kuitenkin hyvää harjoittelua luokkien käyttämisestä Javascript ympäristössä.

Viikon 1 tiedostoja pääsee tarkastelemaan [tämän linkin kautta](https://gitlab.com/Azaziah/komponenttiohjelmointi-olli-suutari/tree/master/vk1), tai klikkamalla polkua Gitlabissa.