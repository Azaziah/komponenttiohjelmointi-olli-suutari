// MongoDB test tutoriaali:
// https://semaphoreci.com/community/tutorials/a-tdd-approach-to-building-a-todo-api-using-node-js-and-mongodb

"use strict";
var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
var mongoose = require('mongoose');
require('sinon-mongoose');

// Haetaan student skeema testausta varten.
var Student = require('../schemas/student_schema');

describe("Kaikkien opiskelijoiden haku", function(){
    // Test will pass if we get all Students
    it("Pitäisi hakea  kaikki opiskelijat.", function(done){
        var StudentMock = sinon.mock(Student);
        var expectedResult = {status: true, Student: []};
        StudentMock.expects('find').yields(null, expectedResult);
        Student.find(function (err, result) {
            StudentMock.verify();
            StudentMock.restore();
            expect(result.status).to.be.true;
            done();
        });
    });

    // Test will pass if we fail to get a Student
    it("Pitäisi tarvittaessa palauttaa virhe.", function(done){
        var StudentMock = sinon.mock(Student);
        var expectedResult = {status: false, error: "Something went wrong"};
        StudentMock.expects('find').yields(expectedResult, null);
        Student.find(function (err, result) {
            StudentMock.verify();
            StudentMock.restore();
            expect(err.status).to.not.be.true;
            done();
        });
    });
});

// Test will pass if the Student is deleted based on an ID
describe("Opiskelijan poistaminen ID:n perusteella.", function(){
    it("Pitäisi poistaa opiskelija ID:n mukaan.", function(done){
        var StudentMock = sinon.mock(Student);
        var expectedResult = { status: true };
        StudentMock.expects('remove').withArgs({_id: 12345}).yields(null, expectedResult);
        Student.remove({_id: 12345}, function (err, result) {
            StudentMock.verify();
            StudentMock.restore();
            expect(result.status).to.be.true;
            done();
        });
    });
    // Test will pass if the Student is not deleted based on an ID
    it("Pitäisi palauttaa virhe, jos poistaminen epäonnistui.", function(done){
        var StudentMock = sinon.mock(Student);
        var expectedResult = { status: false };
        StudentMock.expects('remove').withArgs({_id: 12345}).yields(expectedResult, null);
        Student.remove({_id: 12345}, function (err, result) {
            StudentMock.verify();
            StudentMock.restore();
            expect(err.status).to.not.be.true;
            done();
        });
    });
});