"use strict";
var chai = require('chai');
var expect = chai.expect;

var hello = require('../for_practice_unused/hello.js');
describe('Hello module', () => {
    it('Pitäisi palauttaa "Hello World!"', () =>
    {
        const result = hello;
        expect(result).to.equal("Hello World!");
    });
});