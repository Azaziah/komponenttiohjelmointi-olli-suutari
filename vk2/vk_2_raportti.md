### Olli Suutari | Komponenttiohjelmointi vk 2


### Viikon 2 raportti

Tällä viikolla saimme toteutettavaksemme seuraavia vaatimuksia:
#### Ohjelmiston vaatimukset
- Uuden opiskelijan luonti
- Ohjelmiston tulee palauttaa funktion paluuarvona  järjestelmään luodun opiskelijan etunimi ja sukunimi.
- Opiskelija tulee olla haettavissa sukunimen ja   
opiskelijanumeron perusteella.

Lisäksi meidän piti harjoitella Mocha/Chai testikirjastojen käyttöä.

Päädyin tekemään sovellukseen backendin lisäksi myös frontendin, en tosin ole varma oliko tätä vielä tarkoitus tehdä tässä vaiheessa.

Sovelluksessa voi (localhost//:8000):
- Lisätä opiskelijoita
- Poistaa opiskelijoita
- Tarkastella valitun opiskelijan tietoja
- Rajata näytettäviä opiskelijoita sukunimen ja opiskelijanumeron mukaan.

Tähän käytettyjä teknologioita:
- Node.js
- MongoDB (normaali lokaali asennus, en saanut Dockerilla toimimaan)
- Angular.js
- Jquery
- Bootstrap 4

Pohdintaa
- Kokemus kesätöistä auttoi toteutuksessa huomattavasti
- Sovelluksen vaatimukset olivat jokseenkin hankalia toteuttaa, kuvittelisin niiden tuottaneen ongelmia osalle opiskelijoista.
- Hyvää harjoitusta verkkosovellusten toteutukseen
- Mukava saada perusymmärrys js pohjaiseen yksikkötestaukseen, näitä kannattaa toki harjoitella lisää.
- Jos sovellusta olisi hionut vielä hieman pidemmälle, olisi siitä tullut jo melko hieno toteutus
- Aikaa kului enemmän itse sovelluksen toteuttamiseen kuin testauksen harjoitteluun.
- Tehtävänanto 18 min videon sijaan ehkä mieluummin kirjallisena?

Ilmeisesti tästä oli tarkoitus kirjoittaa myös kaiken kattava tutoriaali? Suuntaan seuraavaksi nukkumaan, joten tutoriaalit jääkööt sikseen, lisäksi taisin tehdä tehtävän hieman eri tavalla kuin oli tarkoitus, siksi siitä ei välttämättä olisi monille hyötyä ja se veisi paljon aikaa.

Kirjoitettakoon kuitenkin perusohjeet lokaalin MongoDB:n käyttöön.

Edit 13.9. Yhteys Mongoon Dockerin kautta normaalin lokaalin MongoDB:n sijaan.

Viikon 2 tiedostoja pääsee tarkastelemaan [tämän linkin kautta](https://gitlab.com/Azaziah/komponenttiohjelmointi-olli-suutari/tree/master/vk2), tai klikkamalla polkua Gitlabissa.