const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const db = require('./db');

db.init();

app.use('/', express.static(__dirname + '/client'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/client/index.html'));
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const router = express.Router();
const main_router = require('./routers/main_router.js');
app.use('/', main_router);

const PORT = 8000;
const HOST = '0.0.0.0';

app.listen(PORT, HOST, function () {
   console.log("\nKuunnellaan osoitetta http://" + HOST + ":" + PORT)
});