// Yhden luokan exporttaus
// tiedostosta person.js
"use strict";

class Person {
  constructor(firstName, lastName, usedFirstName, birthYear) {
    this.firstname = firstName;
    this.lastname = lastName;
    this.usedFirstName = usedFirstName;
    this.birthYear = birthYear;
  }

  introduce() {
    return this.get_PersonDetails();
  }

  get_Name() {
    return this.firstname + " " + this.lastname;
  }
  get_PersonDetails()
  {
    return '\nHello my name is: ' +  this.get_Name() + '<br>I use: ' + this.usedFirstName +
        ' as my calling name.<br>I was born in: ' + this.birthYear + '<br>';
  }
}

module.exports = Person;